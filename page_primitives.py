__all__ = [
    "DocumentPageDimension",
    "DocumentPageRegions",
]

from typing import List, Union, Optional
from uuid import UUID
from pydantic import BaseModel
from .form_region import FormRegion
from .region import TableRegion, WordRegion, Region


class DocumentPageDimension(BaseModel):
    width: Union[float, int]
    height: Union[float, int]


class DocumentPageRegions(BaseModel):
    source: Optional[str]
    version: Optional[str]
    table_regions: Optional[List[TableRegion]]
    form_regions: Optional[List[FormRegion]]
    address_regions: Optional[List[WordRegion]]
    barcode_qr_regions: Optional[List[Region]]
    logo_regions: Optional[List[Region]]
    stamp_regions: Optional[List[Region]]
    signature_regions: Optional[List[Region]]
