
from typing import List, Union, Dict, Any
from enum import Enum
from uuid import UUID
from pydantic import BaseModel

class OCRRecognitionCategory(str, Enum):
    GENERIC                 = "generic"
    NUMERIC                 = "numeric"
    AMOUNT                  = "amount"
    ALPHANUMERIC            = "alphanumeric"
    ALPHABETS               = "alphabets"
    ALPHABETS_SMALL         = "alphabets_small"
    ALPHABETS_SYMBOLS       = "alphabets_symbols"
    ALPHANUMERIC_CAPITALS   = "alphanumeric_capitals"
    NUMERIC_DATE            = "numeric_date"
    ALPHANUMERIC_DATE       = "alphanumeric_date"
    CODES                   = "codes"
    DIGITS                  = "digits"
    ENGLISH                 = "english"
