from ..box import Box


class WordMethods:
    def to_csv_data(self, **kwargs):
        x, y, X, Y = self.bounding_box.to_bbox().to_xyXY()
        if "from_height" in kwargs and "from_width" in kwargs:
            from_h, from_w = kwargs["from_height"], kwargs["from_width"]
            x = x / from_w
            y = y / from_h
            X = X / from_w
            Y = Y / from_h
        data = self.dict(exclude_none=True)
        del data["bounding_box"]
        return {"x": x, "y": y, "X": X, "Y": Y, **data}

    @classmethod
    def from_csv_data(cls, row, **kwargs):
        return cls(
            text=row.text,
            bounding_box=Box.from_bbox(row.x, row.y, row.X, row.Y),
            score=kwargs.get("score"),
        )

    def __repr__(self):
        bb = self.bounding_box
        return f"„{self.text}“"
