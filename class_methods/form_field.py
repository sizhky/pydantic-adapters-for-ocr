import json
from functools import lru_cache
from ..utils.getters import AttrDict
from ..region import WordRegion, Box, Word
from ..utils.azure_converters import (
    bnd_rgn_to_csv_info,
    az_page_dimensions,
    union,
    az_document_csv_data,
)
from ..utils.bounding_boxes import merge_by_bb, pd
from ..fields import FieldClass


class FormFieldMethods:
    @classmethod
    def from_kv_strings(cls, key, value):
        return cls(field_value=WordRegion(text=value), field_class=FieldClass(key))

    @classmethod
    def form_field_list_from_azure_json(cls, az, page_number, doc_page, **kwargs):
        az = AttrDict(az)
        az_document_data = az_document_csv_data(az)
        page_dimensions = az_page_dimensions(az)
        x = az.key_value_pairs
        data = []
        for az_kv_pair in x:
            _data = cls.from_azure_kv_data(
                az_kv_pair, page_dimensions, az_document_data, page_number, doc_page
            )
            data.append(_data) if _data is not None else ...
        return data

    @classmethod
    def from_azure_kv_data(
        cls, az_kv_pair, page_dimensions, az_document_data, page_number, doc_page
    ):
        field_key, field_value, field_pair, field_confidence = get_field_details(
            az_kv_pair, page_number, page_dimensions, doc_page, az_document_data
        )
        return (
            cls(
                field_key=field_key,
                field_value=field_value,
                field_pair=field_pair,
                field_confidence=field_confidence,
            )
            if field_key is not None
            else None
        )

    def to_csv_data(self):
        o = []
        fk = self.field_key
        fv = self.field_value

        for field_ in [self.field_key, self.field_value, self.field_pair]:
            if field_ is None:
                continue
            if field_.words is None:
                x, y, X, Y = field_.bounding_box.to_xyXY()
                label = field_.label
                o.append(dict(x=x, y=y, X=X, Y=Y, readable_label=label, text=""))
            else:
                for word in field_.words:
                    _o = word.to_csv_data()
                    _o["readable_label"] = label = field_.label
                    if "key" in label:
                        deltas = delta_to(fv, from_=word)
                    elif "value" in label:
                        deltas = delta_to(fk, from_=word)
                    else:
                        deltas = {}
                    _o = {**_o, **deltas}
                    o.append(_o)
        return pd.DataFrame(o)


def delta_to(target, from_):
    output = dict(dx=0, dy=0, dX=0, dY=0)
    if target is None:
        return output
    target_bb = tx, ty, tX, tY = target.bounding_box.to_xyXY()
    source_bb = sx, sy, sX, sY = from_.bounding_box.to_xyXY()
    deltas = dict(dx=tx - sx, dy=ty - sy, dX=tX - sX, dY=tY - sY)
    output = {**output, **deltas}
    # from icecream import ic
    # ic(target.dict(exclude_none=True), from_.dict(exclude_none=True), deltas)
    return output


def get_field_details(
    az_kv_pair, page_number, page_dimensions, doc_page, az_document_data
):
    field_key = _do(az_kv_pair, "key", page_number, page_dimensions, doc_page)
    confidence = az_kv_pair.confidence
    if field_key is None:
        return field_key, None, None, confidence
    field_value = _do(az_kv_pair, "value", page_number, page_dimensions, doc_page)
    if field_value is None:
        return field_key, None, None, confidence
    # if field_value.text == "9,600,00 INR":
    #     print(field_value.text)
    #     print(az_kv_pair)
    prefix = ""
    # _az_document_data = az_document_data.query(
    #     "text == @field_value.text"
    # )  # FILTER CRITERIA IS, TEXT SHOULD BE IDENTICAL
    # prefix = (
    #     f"{_az_document_data.readable_label.values[0]}_"
    #     if len(_az_document_data) > 0
    #     else ""
    # )
    field_key.label = f"{prefix}key"
    field_value.label = f"{prefix}value"
    field_pair = WordRegion(
        text=field_key.text + " " + field_value.text,
        bounding_box=Box.from_bbox(
            union(
                field_key.bounding_box.to_xyXY(),
                field_value.bounding_box.to_xyXY(),
            ),
        ),
        label=f"{prefix}kv_pair",
    )
    return field_key, field_value, field_pair, confidence


def _do(az_kv_pair, type_, page_number, page_dimensions, doc_page):
    assert type_ in {"key", "value"}, f"{type_} not `key` or `value`"

    if az_kv_pair[type_] is None:
        return
    page, *bb = bnd_rgn_to_csv_info(
        az_kv_pair[type_].bounding_regions, page_dimensions
    )[0]
    if page != page_number:
        return
    text = az_kv_pair[type_].content
    confidence = az_kv_pair.confidence
    page_csv_data = doc_page.to_csv_data()
    to_find = pd.DataFrame([bb], columns=[*"xyXY"])
    merged = merge_by_bb(
        page_csv_data, to_find, suffixes=("", "_from_kv"), iou_threshold=1e-3
    )
    words = []
    for _, row in merged.iterrows():
        words.append(Word.from_csv_data(row, score=confidence))
    return WordRegion(
        text=text,
        words=words,
        bounding_box=Box.from_bbox(*bb),
        label=type_,
        score=confidence,
    )
