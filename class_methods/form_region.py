from ..utils.getters import AttrDict
from ..utils.bounding_boxes import to_relative
from ..form_field import FormField
from ..box import Box
from ..utils.azure_converters import az_page_dimensions
import pandas as pd


class FormRegionMethods:
    @classmethod
    def from_kv_dict(cls, dict, version=None, source=None, form_name=None):
        form_fields = [
            FormField.from_kv_strings(k, v)
            for k, v in dict.items()
            if v is not None and v != ""
        ]
        return cls(
            form_fields=form_fields, form_name=form_name, version=version, source=source
        )

    @classmethod
    def from_azure_json(cls, az, page, doc_page, **kwargs):
        
        form_fields = FormField.form_field_list_from_azure_json(
            az, page, doc_page, **kwargs
        )
        return cls(
            form_name="AzureFormInformation",
            form_fields=form_fields,
            version=-1,
            source="azure",
        )

    def to_csv_data(self):
        o = []
        for form_field in self.form_fields:  # TODO: GET RID OF THIS
            _o = form_field.to_csv_data()
            _o["region_name"] = self.form_name
            o.append(_o)
        if len(o) == 0:
            return pd.DataFrame()
        return pd.concat(o).reset_index(drop=True)
    