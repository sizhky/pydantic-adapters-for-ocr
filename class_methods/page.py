import pandas as pd
from ...image import Image, Point, QuadrilateralBoundingBox
from ..region import Box
from ..page_primitives import DocumentPageDimension
from ..utils.loaders import load_csv_data
from ..region import Word
from ..ocr import OCRResults


class DocumentPageMethods:
    @classmethod
    def from_azure_words(cls, az, page_number, **kwargs):
        if "height" in kwargs and "width" in kwargs:
            page_dimension = DocumentPageDimension(
                width=kwargs["width"], height=kwargs["height"]
            )
        else:
            page_dimension = None
        words = []
        for word in az:
            word = get_azure_word(word)
            words.append(word)
        ocr_results = {"words": words}
        return cls(
            ocr_results=ocr_results,
            page_dimension=page_dimension,
            page_number=page_number,
            **kwargs,
        )

    @classmethod
    def from_csv_data(cls, *, page_number, image=None, csv_data=None, **kwargs):
        if image is not None:
            image = Image(content=image)
            h, w = image.shape[:2]
            page_dimension = DocumentPageDimension(height=h, width=w)
        else:
            page_dimension = None

        words = (
            None
            if csv_data is None
            else csv_to_words(csv_data, page_number=page_number)
        )
        ocr_results = OCRResults(words=words)

        return cls(
            image=image,
            page_dimension=page_dimension,
            ocr_results=ocr_results,
            page_number=page_number,
            **kwargs,
        )

    def dict(self, *args, **kwargs):
        kwargs.pop("exclude_none", ...)
        kwargs.pop("exclude", ...)
        return super().dict(*args, **kwargs, exclude_none=True, exclude={"image"})

    def to_csv_data(self, **kwargs):
        csv_data = []
        height, width = self.page_dimension.height, self.page_dimension.width
        for word in self.ocr_results.words:
            csv_data.append(
                {
                    **word.to_csv_data(from_height=height, from_width=width, **kwargs),
                    "page": self.page_number,
                }
            )
        return pd.DataFrame(csv_data)


def get_azure_word(word):
    ps = {
        f"p{i}": Point(**{k: v for k, v in p.items()})
        for i, p in enumerate(word["polygon"], 1)
    }
    bbox = Box(qbox=QuadrilateralBoundingBox(**ps))
    return Word(
        text=word["content"],
        bounding_box=bbox,
    )


def csv_to_words(
    csv_data, page_number, must_have_columns=None, height=None, width=None
):
    must_have_columns = (
        [*"xyXY", "text"] if must_have_columns is None else must_have_columns
    )
    csv_data = load_csv_data(csv_data, must_have_columns)
    if "page" in csv_data.columns:
        csv_data = csv_data.query("page == @page_number")
    words = []
    for _, row in csv_data.iterrows():
        text = row.text
        if text is None or text != text:
            continue
        x, y, X, Y = row.x, row.y, row.X, row.Y
        if height is not None and width is not None:
            x, X = x / width, X / width
            y, Y = y / height, Y / height

        confidence = -1 if "confidence" not in row else row.confidence
        prediction = row.pred if "pred" in row else None
        if "readable_label" in row:
            label = row.readable_label
        elif "label" in row:
            label = row.label
        else:
            label = None
        words.append(
            Word(
                bounding_box=Box.from_bbox(x, y, X, Y),
                text=text,
                score=confidence,
                label=label,
                prediction=prediction,
            ),
        )
    return words
