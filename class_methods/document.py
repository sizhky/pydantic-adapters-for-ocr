from ..page import DocumentPage
from ..utils.loaders import load_csv_data, read_json
from ..utils.getters import AttrDict, AD
from ..utils.bounding_boxes import to_relative
import pandas as pd
from pathlib import Path
from ..page_primitives import DocumentPageRegions, TableRegion, FormRegion
import json


class DocumentMethods:
    @classmethod
    def from_key_value_json(cls, input, version, source, form_name=None):
        """
        fpath should look like -
            {'vendor_gstin': '19BGEPS3292R1ZE',
            'vendor_name': 'SARKAR AND SONS',
            'invoice_date': '2023-10-11',
            'po_number': '5000109735',
            'invoice_amount': 221.0,
            'invoice_number': 'PO/OCT/20/23-24',
            'buyer_gstin': '',
            'buyer_name': '',
            'currency': 'INR'}
        """
        kvs = read_json(input) if isinstance(input, (str, Path)) else input
        form_region = FormRegion.from_kv_dict(kvs, version, source)
        doc_page = DocumentPage(
            page_number=-1, regions=DocumentPageRegions(form_regions=[form_region])
        )
        return cls(document_pages=[doc_page])

    @classmethod
    def from_azure_json(cls, fpath_or_az, **kwargs):
        if isinstance(fpath_or_az, (str, Path)):
            az = read_json(fpath_or_az)
        else:
            az = fpath_or_az
        document_pages = get_document_pages(az)
        return cls(document_pages=document_pages, **kwargs)

    @classmethod
    def from_modified_azure_json(cls, fpath_or_az, **kwargs):
        if isinstance(fpath_or_az, (str, Path)):
            az = read_json(fpath_or_az)
        else:
            az = fpath_or_az
        document_pages = get_document_pages_from_item_level_json(fpath_or_az)
        return document_pages

    @classmethod
    def from_csv_data(cls, images, csv_data, **kwargs):
        document_pages = []
        csv_data = load_csv_data(csv_data, must_have_columns=[*"xyXY", "text", "page"])
        for page in sorted(csv_data["page"].unique()):
            image = images[page] if images is not None else None
            document_pages.append(
                DocumentPage.from_csv_data(
                    image=image,
                    csv_data=csv_data.query("page == @page"),
                    page_number=page,
                )
            )
        return cls(document_pages=document_pages, **kwargs)

    def to_csv_data(self, height=None, width=None):
        doc_csvs = []
        for doc_data in self.document_pages:
            doc_csvs.append(doc_data.to_csv_data(height=height, width=width))
        return pd.concat(doc_csvs)

    def get_form_fields(self):
        o = []
        for page, doc_page in enumerate(self.document_pages):
            for form_region in doc_page.regions.form_regions:
                _o = form_region.to_csv_data()
                if _o.shape[0] > 0:
                    _o["page"] = page
                    o.append(_o)
        return pd.concat(o)

    def get_modified_form_fields(doc_page, page=0):
        o = []
        for form_region in doc_page.regions.form_regions:
            _o = form_region.to_csv_data()
            if _o.shape[0] > 0:
                _o["page"] = page
                o.append(_o)
        return pd.concat(o)

    def to_csv_data_with_form_fields(self, height=None, width=None):
        doc_csv = self.to_csv_data()
        doc_fields = self.get_fields()


def get_document_pages(fpath_or_az, create_form_regions=True):
    if isinstance(fpath_or_az, (str, Path)):
        az = read_json(fpath_or_az)
    else:
        az = fpath_or_az

    document_pages = []
    pages = az["pages"]
    for page_number, page_data in enumerate(pages):
        height = page_data["height"]
        width = page_data["width"]
        words = page_data["words"]
        doc_page = DocumentPage.from_azure_words(
            words,
            page_number=page_number,
            height=height,
            width=width,
        )
        if create_form_regions:
            form_region = FormRegion.from_azure_json(
                az, page=page_number, doc_page=doc_page
            )
            regions = DocumentPageRegions(
                table_regions=None, form_regions=[form_region]
            )
            doc_page = DocumentPage.from_azure_words(
                words,
                page_number=page_number,
                height=height,
                width=width,
                regions=regions,
            )
        document_pages.append(doc_page)
    return document_pages


def get_document_pages_from_item_level_json(fpath_or_az, create_form_regions=True):
    if isinstance(fpath_or_az, (str, Path)):
        az = read_json(fpath_or_az)
    else:
        az = fpath_or_az
    document_pages = []
    az_pages = az["pages"]
    global_page_count = 0
    for pg_no, az_page in enumerate(az_pages):
        for page, page_data in enumerate(az_page):
            height = page_data["height"]
            width = page_data["width"]
            words = page_data["words"]
            doc_page = DocumentPage.from_azure_words(
                words,
                page_number=page + global_page_count,
                height=height,
                width=width,
            )
            if create_form_regions:
                form_region = FormRegion.from_azure_json(
                    az, page=page + global_page_count, doc_page=doc_page
                )
                # print(form_region)
                regions = DocumentPageRegions(
                    table_regions=None, form_regions=[form_region]
                )
            else:
                regions = None
            doc_page = DocumentPage.from_azure_words(
                words,
                page_number=page + global_page_count,
                height=height,
                width=width,
                regions=regions,
            )
            global_page_count = global_page_count + 1
            document_pages.append(doc_page)
    return document_pages
