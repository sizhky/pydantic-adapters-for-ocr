from typing import Optional
from pydantic import BaseModel

from ..image import (
    BoundingBox,
    QuadrilateralBoundingBox,
    PolygonBoundingBox,
    Point,
)


class Box(BaseModel):
    bbox: Optional[BoundingBox]
    qbox: Optional[QuadrilateralBoundingBox]
    polybox: Optional[PolygonBoundingBox]
    is_absolute: bool = True

    def to_qbox(cls):
        raise NotImplementedError("Yet to Implment")

    @classmethod
    def from_qbox(cls, input):
        raise NotImplementedError("Yet to Implment")

    def to_poly_box(cls):
        raise NotImplementedError("Yet to Implment")

    @classmethod
    def from_poly_box(cls, input):
        raise NotImplementedError("Yet to Implment")

    def to_bbox(self):
        if self.bbox is not None:
            return self.bbox
        if self.qbox is not None:
            points = self.qbox.dict().values()
            xs = [p["x"] for p in points]
            ys = [p["y"] for p in points]
            x, X = min(xs), max(xs)
            y, Y = min(ys), max(ys)
            is_absolute = any(i > 1 for i in [x, y, X, Y])
            return Box(
                bbox=BoundingBox(topLeft=Point(x=x, y=y), bottomRight=Point(x=X, y=Y)),
                is_absolute=is_absolute,
            )
        raise NotImplementedError("Yet to Implment")

    @classmethod
    def from_bbox(cls, *input):
        if len(input) == 1:
            input = input[0]
        x, y, X, Y = input
        is_absolute = any([i > 1 for i in input])
        return cls(
            bbox=BoundingBox(topLeft=Point(x=x, y=y), bottomRight=Point(x=X, y=Y)),
            is_absolute=is_absolute,
        )

    def to_xyXY(self):
        return self.to_bbox().to_xyXY()

    def __repr__(self):
        x, y, X, Y = self.to_bbox().to_xyXY()
        return f"@`{x},{y},{X},{Y}`"
