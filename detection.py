from typing import List, Union
from enum import Enum
from uuid import UUID
from typing import Dict
from pydantic import BaseModel

from ..image import Image, BoundingBox, QuadrilateralBoundingBox, PolygonBoundingBox
from ..detection.detection import Detection, DetectionCategory


class OCRDetectionCategory(str, Enum):
    TEXT = DetectionCategory.TEXT.value
    QRCODE = DetectionCategory.QRCODE.value
    BARCODE = DetectionCategory.BARCODE.value
    CHECKBOX = DetectionCategory.CHECKBOX.value
    GRAPHICAL_ELEMENT = DetectionCategory.GRAPHICAL_ELEMENT.value
    LOGO = DetectionCategory.LOGO.value
    FIGURE = DetectionCategory.FIGURE.value
    EQUATION = DetectionCategory.EQUATION.value
    SIGNATURE = DetectionCategory.SIGNATURE.value
    STAMP = DetectionCategory.STAMP.value


class OCRDetection(BaseModel):
    id: Union[UUID, int] = None
    category: OCRDetectionCategory
    detection: Union[BoundingBox, QuadrilateralBoundingBox, PolygonBoundingBox]
