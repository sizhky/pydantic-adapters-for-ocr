from pathlib import Path
import pandas as pd
import json


def load_csv_data(csv_data, must_have_columns):
    if isinstance(csv_data, (Path, str)):
        assert Path(csv_data).exists(), f"File is not present at {csv_data}"
        csv_data = str(csv_data)
        csv_data = (
            pd.read_csv(csv_data)
            if csv_data.endswith("csv")
            else pd.read_parquet(csv_data)
        )
    assert all(c in csv_data.columns for c in must_have_columns)
    if 'page' in csv_data.columns:
        csv_data.loc[:,'page'] = csv_data['page'].map(int)
    return csv_data

def read_json(fpath):
    with open(fpath, "r") as f:
        return json.load(f)