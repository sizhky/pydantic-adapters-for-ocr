from .getters import AttrDict
import pandas as pd


def az_page_dimensions(az):
    """Return a dictionary of
    (height, width) tuples for each
    page (start counting from 0)"""
    return {(y.page_number) - 1: (y.height, y.width) for y in az.pages}


def bnd_rgn_to_csv_info(bnd_rgns, page_dimensions):
    info = []
    for bnd_rgn in bnd_rgns:
        page = bnd_rgn.page_number - 1  # azure numbers fields from 1 instead of 0
        h, w = page_dimensions[int(page)]
        xs = [pt.x / w for pt in bnd_rgn.polygon]
        ys = [pt.y / h for pt in bnd_rgn.polygon]
        x, X = min(xs), max(xs)
        y, Y = min(ys), max(ys)
        info.append((page, x, y, X, Y))
    return info


def union(b1, b2):
    a, b, c, d = b1
    p, q, r, s = b2
    return min(a, p), min(b, q), max(c, r), max(d, s)


def az_fields_to_form_data(az):
    az = AttrDict(az)
    x = az.key_value_pairs


def az_fields_to_csv_data(az):
    az = AttrDict(az)
    page_dimensions = az_page_dimensions(az)
    x = az.key_value_pairs

    o = []
    for kv_data in x:
        info = ()
        info += ("key", kv_data.key.content, kv_data.confidence)
        bnd_rgns = kv_data.key.bounding_regions
        if len(bnd_rgns) > 1:
            print(f'WARNING :: "More than one bounding region found"')
        _bnd_regions = bnd_rgn_to_csv_info(bnd_rgns, page_dimensions)
        infos = [(*info, *bnd_rgn) for bnd_rgn in _bnd_regions]
        key_box = _bnd_regions[0][1:]
        o.extend(infos)
        if kv_data.value is not None:
            info = ()
            info += ("value", kv_data.value.content, kv_data.confidence)
            bnd_rgns = kv_data.value.bounding_regions
            if len(bnd_rgns) > 1:
                print(f'WARNING :: "More than one bounding region found"')
            _bnd_regions = bnd_rgn_to_csv_info(bnd_rgns, page_dimensions)
            infos = [(*info, *bnd_rgn) for bnd_rgn in _bnd_regions]
            val_box = _bnd_regions[0][1:]
            page = _bnd_regions[0][0]
            o.extend(infos)

            info = ()
            info += ("kv_pair", None, kv_data.confidence)
            kv_box = union(key_box, val_box)
            info += (page, *kv_box)
            o.append(info)

    return pd.DataFrame(
        o, columns=["readable_label", "text", "confidence", "page", "x", "y", "X", "Y"]
    )


def az_document_csv_data(az):
    az = AttrDict(az)
    page_dimensions = az_page_dimensions(az)
    x = az.documents[0]
    o = []
    for fld, val in x.fields.items():
        info = ()
        info += (fld, val.content, val.confidence)
        bnd_rgns = val.bounding_regions
        if len(bnd_rgns) > 1:
            print(f'WARNING :: "More than one bounding region found"')
        infos = [
            (*info, *bnd_rgn)
            for bnd_rgn in bnd_rgn_to_csv_info(bnd_rgns, page_dimensions)
        ]
        o.extend(infos)
    return pd.DataFrame(
        o, columns=["readable_label", "text", "confidence", "page", "x", "y", "X", "Y"]
    )
