from json import JSONEncoder


def nested_get(model, *path):
    """
    Recursively retrieves a nested value from a model using a path.
    Useful when you want to access items programatically

    Args:
        model: The input model, typically a dictionary or object.
        *path: A variable number of path elements to traverse the nested structure.

    Returns:
        The value found at the specified path within the model. If the path is not valid,
        or if the value is not found, it returns None.

    Examples:
        model = {
            'a': {
                'b': {
                    'c': 42
                }
            }
        }

        value = nested_get(model, 'a', 'b', 'c')  # Returns 42
        value = nested_get(model, 'x', 'y', 'z')  # Returns None
    """
    o = model
    for p in path:
        if isinstance(p, str):
            try:
                o = o.get(p)
            except:
                o = getattr(o, p)
        else:
            o = o[p]
        if o is None:
            return
    return o


def _default(self, obj):
    return getattr(obj.__class__, "to_json", _default.default)(obj)


_default.default = JSONEncoder().default
JSONEncoder.default = _default


def isnamedtupleinstance(x):
    _type = type(x)
    bases = _type.__bases__
    if len(bases) != 1 or bases[0] != tuple:
        return False
    fields = getattr(_type, "_fields", None)
    if not isinstance(fields, tuple):
        return False
    return all(type(i) == str for i in fields)


def unpack(obj):
    if isinstance(obj, dict):
        return {key: unpack(value) for key, value in obj.items()}
    elif isinstance(obj, list):
        return [unpack(value) for value in obj]
    elif isnamedtupleinstance(obj):
        return {key: unpack(value) for key, value in obj._asdict().items()}
    elif isinstance(obj, tuple):
        return tuple(unpack(value) for value in obj)
    else:
        return obj


class AttrDict(object):
    """
    Utility class to interact with a dictionary as if it were an object. `AD` is an alias to this class

    FEATURES:
    0. Access and modify keys (including nested keys) as if they were object attributes, supporting tab-completion.
       Example: `self.key1.key2[0].key3`
    1. Keys and values are recursively converted to AttrDict instances.
    2. Pretty-print the dictionary using `self.pretty()`.
    3. Convert the entire structure to a regular dictionary at any time using `self.to_dict()`.
    3. Recursively remove keys using `self.drop(key)` from a JSON object.
    4. Apply a function to all values at all levels using `map`.

    GOTCHAS:
    1. All integer keys are implicitly converted to strings due to the enforced `self.key` format.
    2. You can still use `self[int]`, but this internally converts the integer to a string.

    METHODS:
    - `items()`: Return the items of the AttrDict as key-value pairs.
    - `keys()`: Return the keys of the AttrDict.
    - `values()`: Return the values of the AttrDict.
    - `update(dict)`: Update the AttrDict with key-value pairs from another dictionary.
    - `get(key, default=None)`: Get the value associated with a key, with an optional default value.
    - `__iter__()`: Allow iteration over the keys of the AttrDict.
    - `__len__()`: Return the number of keys in the AttrDict.
    - `__repr__()`: Return a string representation of the AttrDict.
    - `__dir__()`: List the keys of the AttrDict as attributes.
    - `__contains__(key)`: Check if a key exists in the AttrDict.
    - `__delitem__(key)`: Delete a key from the AttrDict.
    - `map(func)`: Apply a function to all values in the AttrDict.
    - `drop(key)`: Recursively remove a key and its values from the AttrDict.
    - `to_dict()`: Convert the AttrDict and its nested structure to a regular dictionary.
    - `pretty(print_with_logger=False, *args, **kwargs)`: Pretty-print the AttrDict as JSON.
    - `__eq__(other)`: Compare the AttrDict with another dictionary for equality.
    - `find_address(key, current_path="")`: Find and return all addresses (paths) of a given key in the AttrDict.
    - `summary(current_path='', summary_str='', depth=0, sep='\t')`: Generate a summary of the structure and values in the AttrDict.
    - `write_summary(to, **kwargs)`: Write the summary to a file or stream.
    - `fetch(addr)`: Retrieve a value at a specified address (path).

    PARAMETERS:
    - `data` (dict, optional): Initial data to populate the AttrDict.

    USAGE:
    - Create an AttrDict instance by providing an optional initial dictionary, and then access and manipulate its contents as if they were object attributes.

    EXAMPLE:
    ```python
    my_dict = {'name': 'John', 'age': 30, 'address': {'city': 'New York', 'zip': '10001'}}
    attr_dict = AttrDict(my_dict)
    print(attr_dict.name)  # Access values like attributes
    attr_dict.address.city = 'Los Angeles'  # Modify nested values
    ```
    """

    def __init__(self, data=None, **kwargs):
        if data is None:
            data = {}
        data = {**data, **kwargs}
        for name, value in data.items():
            setattr(self, str(name), self._wrap(value))

    def items(self):
        return self.__dict__.items()

    def keys(self):
        return self.__dict__.keys()

    def values(self):
        return self.__dict__.values()

    def _wrap(self, value):
        if isinstance(value, (list, tuple, list, set, frozenset)):
            value = type(value)([self._wrap(v) for v in value])
            if isinstance(value, (list, list)):
                value = list(value)
            return value
        else:
            return AttrDict(value) if isinstance(value, dict) else value

    __getitem__ = (
        lambda self, x: AttrDict({_x: self[_x] for _x in x})
        if isinstance(x, (list, list))
        else getattr(self, str(x))
    )
    __setitem__ = lambda self, k, v: setattr(self, str(k), self._wrap(v))

    def update(self, dict):
        for k, v in dict.items():
            self[k] = v

    def get(self, key, default=None):
        key = str(key)
        return self[key] if key in self else default

    def __iter__(self):
        return iter(self.keys())

    def __len__(self):
        return len(self.keys())

    def __repr__(self):
        return "{%s}" % str(
            ", ".join("'%s': %s" % (k, repr(v)) for (k, v) in self.__dict__.items())
        )

    def __dir__(self):
        return self.__dict__.keys()

    def __contains__(self, key):
        key = str(key)
        return key in self.__dict__.keys()

    def __delitem__(self, key):
        key = str(key)
        del self.__dict__[key]

    def map(self, func):
        for k in dir(self):
            v = self[k]
            if isinstance(v, AttrDict):
                v.map(func)
            elif isinstance(v, (list, tuple, list, set, frozenset)):
                v = [_v.map(func) if isinstance(_v, AttrDict) else func(_v) for _v in v]
            else:
                v = func(v)
            self[k] = v

    def drop(self, key):
        if key in self:
            del self[key]
        for k in dir(self):
            v = self[k]
            if isinstance(v, AttrDict):
                v.drop(key)
            if isinstance(v, (list, tuple, list, set, frozenset)):
                v = [_v.drop(key) for _v in v if isinstance(_v, AttrDict)]

    def to_dict(self):
        d = {}
        for k in dir(self):
            v = self[k]
            if isinstance(v, AttrDict):
                v = v.to_dict()
            if isinstance(v, (list, tuple, list, set, frozenset)):
                v = [_v.to_dict() if isinstance(_v, AttrDict) else _v for _v in v]
            d[k] = v
        return d

    def pretty(self, print_with_logger=False, *args, **kwargs):
        pretty_json(
            self.to_dict(), print_with_logger=print_with_logger, *args, **kwargs
        )

    def __eq__(self, other):
        return AttrDict(other).to_dict() == self.to_dict()

    def find_address(self, key, current_path=""):
        addresses = []
        for k in self.keys():
            if current_path:
                new_path = f"{current_path}.{k}"
            else:
                new_path = k

            if k == key:
                addresses.append(new_path)

            if isinstance(self[k], AttrDict):
                addresses.extend(self[k].find_address(key, new_path))

            elif isinstance(self[k], (list, tuple, list, set, frozenset)):
                for i, item in enumerate(self[k]):
                    if isinstance(item, AttrDict):
                        addresses.extend(item.find_address(key, f"{new_path}.{i}"))
        return addresses

    def summary(self, current_path="", summary_str="", depth=0, sep="\t"):
        tab = sep * depth
        for k in self.keys():
            if current_path:
                new_path = f"{current_path}.{k}"
            else:
                new_path = k

            if isinstance(self[k], AttrDict):
                summary_str += f"{tab}{k}\n"
                summary_str = self[k].summary(new_path, summary_str, depth + 1, sep=sep)
            elif isinstance(self[k], (list, tuple, set, frozenset)):
                summary_str += f"{tab}{k}\n"
                for i, item in enumerate(self[k]):
                    summary_str += f"{tab}{sep}{i}\n"
                    if isinstance(item, AttrDict):
                        summary_str = item.summary(
                            f"{new_path}.{i}", summary_str, depth + 2, sep=sep
                        )
                    elif isinstance(item, (list, tuple, set, frozenset)):
                        nested_path = f"{new_path}.{i}"
                        nested_summary_str = ""
                        for j, nested_item in enumerate(item):
                            summary_str += f"{tab}{sep}{sep}{j}\n"
                            if isinstance(nested_item, AttrDict):
                                nested_summary_str = nested_item.summary(
                                    f"{nested_path}.{j}",
                                    nested_summary_str,
                                    depth + 3,
                                    sep=sep,
                                )
                            elif isinstance(nested_item, (list, tuple, set, frozenset)):
                                nested_list_path = f"{nested_path}.{j}"
                                for idx, nested_list_item in enumerate(nested_item):
                                    if isinstance(nested_list_item, AttrDict):
                                        nested_summary_str = nested_list_item.summary(
                                            f"{nested_list_path}.{idx}",
                                            nested_summary_str,
                                            depth + 4,
                                            sep=sep,
                                        )
                        if nested_summary_str:
                            summary_str += nested_summary_str
                    else:
                        summary_str += f"{tab}{sep}{k} - {type(self[k]).__name__}\n"
            else:
                summary_str += f"{tab}{k} - {type(self[k]).__name__}\n"

        return summary_str

    def write_summary(self, to, **kwargs):
        writelines(self.summary(**kwargs).split("\n"), to)

    def fetch(self, addr):
        if isinstance(addr, (list, list)):
            return list([self.fetch(_addr) for _addr in addr])

        o = self
        for p in addr.split("."):
            try:
                o = o[int(p)]
            except:
                o = o[p]
        return o


AD = AttrDict
