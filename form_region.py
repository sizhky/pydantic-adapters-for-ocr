__all__ = ["FormRegion"]

from typing import List, Union, Optional
from uuid import UUID
from .region import Region
from .box import Box
from .class_methods.form_region import FormRegionMethods
from .form_field import FormField
from .box import Box


class FormRegion(FormRegionMethods, Region):
    version: str
    source: str
    id: Union[UUID, int] = None
    form_name: Optional[str] = None
    form_fields: List[FormField]
    bounding_box: Optional[Box]
