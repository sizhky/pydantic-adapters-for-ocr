__all__ = [
    "Box",
    "Region",
    "Word",
    "WordRegion",
    "TableCell",
    "TableRegion",
]

from typing import List, Union, Optional
from uuid import UUID
from pydantic import BaseModel
from .class_methods.word import WordMethods
from .box import Box


class Region(BaseModel):
    region_id: Union[UUID, int] = None
    bounding_box: Box
    score: Optional[float] = None
    label: Optional[str] = None
    prediction: Optional[str] = None
    source: Optional[str] = None
    version: Optional[str] = None


class Word(WordMethods, Region):
    text: str


class WordRegion(Region):
    text: str
    words: Optional[List[Word]]
    bounding_box: Optional[Box]


class Line(WordRegion):
    line_number: Optional[int] = None


class Paragraph(Region):
    lines: List[Line]
    paragraph_number: Optional[int] = None


class TableCell(Region):
    row_id: int
    col_id: int
    cell: WordRegion


class TableRegion(Region):
    header: List[TableCell]
    body: List[List[TableCell]]  # List of all Rows containing list of cells
