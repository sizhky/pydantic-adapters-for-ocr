# pydantic-adapters-for-ocr

A repo demonstrating how to create a suite of pydantic models for the purpose of data storage and interchange

E.g., visit `document.py` for the core class and visit `class_methods/document.py` for the to and fro adapaters for the same pydantic model.

One can theoritically create a spoke and hub framework that uses `Document` as the hub and add support conversion to/from any number of document formats such as

1. Storage formats such as - to/from csv, parquet, sql, mongodb
2. Cloud services such as - from azure, aws, gcp ocr data structures
3. Annotation softwares such as - to/from CVAT, label-studio
