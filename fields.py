from enum import Enum


class FieldClass(str, Enum):
    Generic = "generic"
    InvoiceNumber = "invoice_number"
    ScrollAmount = "invoice_amount"
    VendorGSTIN = "vendor_gstin"
    BuyerGSTIN = "buyer_gstin"
    VendorName = "vendor_name"
    InvoiceDate = "invoice_date"
    PurchaseOrderNumber = "purchase_order_number"
    Currency = "currency"
