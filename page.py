__all__ = [
    "DocumentPageDimension",
    "DocumentPageRegions",
    "DocumentPage",
]

from typing import List, Dict
from typing import Optional
from pydantic import BaseModel
from ..language import Language
from .ocr import OCRResults
from .orientation import PageOrientation
from .image_quality import ImageQuality
from .page_primitives import DocumentPageDimension, DocumentPageRegions
from .class_methods.page import DocumentPageMethods
from ..image import Image


class DocumentPage(DocumentPageMethods, BaseModel):
    page_number: int
    image: Optional[Image] = None
    page_dimension: Optional[DocumentPageDimension] = None
    page_orientation: Optional[PageOrientation] = None
    detected_language: Optional[Language] = None
    regions: Optional[DocumentPageRegions] = None
    ocr_results: Optional[OCRResults] = None
    image_quality: Optional[ImageQuality] = None
    model_versions: Optional[Dict[str, str]] = None  # Dict of {"stage": "version", ...}
