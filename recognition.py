from typing import List, Union, Dict, Any
from enum import Enum
from uuid import UUID
from pydantic import BaseModel

from ..image import Image, BoundingBox, Point
from pydantic import BaseModel

from .detection import OCRDetection
from .text_recognition import TextRecognition

# from .checkbox_recognition import CheckboxRecognition


class OCRRecognition(BaseModel):
    recognition: TextRecognition
