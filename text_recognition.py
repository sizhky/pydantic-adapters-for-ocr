
from typing import List, Union, Dict, Any
from enum import Enum
from uuid import UUID
from pydantic import BaseModel

from .recognition_category import OCRRecognitionCategory

class WordPrediction(BaseModel):
    word: str
    score: float

class Entity(BaseModel):
    entity: List[WordPrediction]

class TextCategoryRecognition(BaseModel):
    category: OCRRecognitionCategory
    entities: List[Entity]

class TextRecognition(BaseModel):
    id: Union[UUID, int] = None
    text: List[TextCategoryRecognition]
