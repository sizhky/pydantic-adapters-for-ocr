__all__ = ["OCRResults"]
from typing import List, Union, Dict, Any, Optional
from enum import Enum
from uuid import UUID
from pydantic import BaseModel

from .detection import OCRDetection
from .recognition import OCRRecognition
from .region import Box, Word, Line, Paragraph


class OCRResults(BaseModel):
    version: Optional[str]
    source: Optional[str]
    words: Optional[List[Word]] = None
    lines: Optional[List[Line]] = None
    paragraphs: Optional[List[Paragraph]] = None


class OCR(BaseModel):
    id: Union[UUID, int] = None
    bounding_box: Box
    text: str
    score: float
