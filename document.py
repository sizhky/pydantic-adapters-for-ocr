__all__ = ["Document"]

from typing import List, Union, Optional
from uuid import UUID
from pydantic import BaseModel
from .page import DocumentPage
from .class_methods.document import DocumentMethods


class Document(DocumentMethods, BaseModel):
    id: Union[UUID, int] = None
    document_pages: List[DocumentPage]
    version: str = "0.02"
    source: Optional[str]
