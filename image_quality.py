__all__ = ["PageDefectTypes", "ImageDefects", "ImageQuality"]

from typing import List, Union, Dict, Any
from enum import Enum
from uuid import UUID
from pydantic import BaseModel


class PageDefectTypes(str, Enum):
    DEFECTIVE_DOCUMENT = "quality/defect_document_cutoff"
    DOCUMENT_GLARE_ISSUE = 'quality/defect_glare"'
    DOCUMENT_TEXT_ISSUE = "quality/defect_text"


class ImageDefects(BaseModel):
    type: PageDefectTypes
    confidence_score: float


class ImageQuality(BaseModel):
    quality_score: float
    detected_defects: List[ImageDefects]
