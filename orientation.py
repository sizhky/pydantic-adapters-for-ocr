__all__ = ["Orientation", "PageOrientation"]

from enum import Enum
from pydantic import BaseModel


class Orientation(str, Enum):
    UP = "PAGE_UP"
    Down = "PAGE_DOWN"
    ClockWise_90 = "90_Clockwise"
    AntiClockWise_90 = "90_AntiClockwise"


class PageOrientation(BaseModel):
    score: float
    original_orientation: Orientation
    current_orientation: Orientation
