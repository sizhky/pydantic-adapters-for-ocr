__all__ = ["FormField"]

from pydantic import BaseModel
from typing import Optional
from .region import WordRegion
from .fields import FieldClass
from .class_methods.form_field import FormFieldMethods


class FormField(FormFieldMethods, BaseModel):
    field_key: Optional[WordRegion]
    field_value: Optional[WordRegion]
    field_pair: Optional[WordRegion]
    field_confidence: Optional[float]
    field_class: Optional[FieldClass]

    class Config:
        use_enum_values = True
